import MyButton from "../ui/MyButton";
import MyInput from "../ui/MyInput";

export default [
    MyButton,
    MyInput,
]